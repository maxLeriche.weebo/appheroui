import React, { lazy, Suspense } from "react";
import { BrowserRouter as Router, Switch, Route, Link,Redirect } from "react-router-dom";
import './App.css'
import './Home/main.css'
import LoadingScreen from "./loading"
const About = lazy(() => import("./About"));
const Home = lazy(() => import("./Home/Home"));
const Nav = lazy(()=> import("./Nav/Nav"));
const LoginForm = lazy(()=> import("./Acc/login"));
const RegisterForm = lazy(()=> import("./Acc//register"));

class App extends React.Component<any,{logedin:boolean}>{
  constructor(props:{logedin:boolean}){
    super(props)
    this.state ={
      logedin:localStorage.getItem("logedin")==null?false:Boolean(localStorage.getItem("logedin"))
    }
    
  }
   logingin= (status:boolean) => {
    localStorage.setItem("logedin",status.toString())
    this.setState({
      logedin: localStorage.getItem("logedin")==null?false:Boolean(localStorage.getItem("logedin"))
    });
  }
  disconect =() => {
    localStorage.clear()
    return(<Redirect to="/" />);
  }
  
  render(){
    return(<Router>
      <Suspense fallback={<LoadingScreen/>}>
      <Nav LogedIn={this.state.logedin} Pseudo="" Image="" />
       <div  id="outer-container">
         <h1  id="navh1" className="navh1" ><a href="/">Apphero</a></h1>
         <div className="menu-item" id="page-wrap">
        <Switch >
          <Route path="/about">
            <About />
          </Route>
          <Route path="/" exact>
            <Home />
          </Route>

          <Route path="/play" exact>
          {this.state.logedin?<Redirect to="/" />:<Redirect to="/" />}
          </Route>
          <Route path="/chat" exact>
          {this.state.logedin?<Redirect to="/" />:<Redirect to="/" />}
          </Route>
          <Route path="/acc" exact>
          {this.state.logedin?<Redirect to="/" />:<Redirect to="/" />}
          </Route>

          <Route path="/register" exact>
            {this.state.logedin?<Redirect to="/" />:<RegisterForm logedin={this.logingin} login={this.state.logedin}/>}
          </Route>
          <Route path="/login" exact>
            {this.state.logedin?<Redirect to="/" />:<LoginForm logedin={this.logingin} login={this.state.logedin}/>}
          </Route>
          <Route path="/disconnect" exact>
            <this.disconect/>
          </Route>
        </Switch>
        </div>
        </div>
      </Suspense>
   </Router>)
  }

}

export default App;