import React, { lazy, Suspense } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSpinner,faEnvelope,faSignInAlt } from '@fortawesome/free-solid-svg-icons'


const LoadingScreen=() =>(
    <div style={{display:"block",marginLeft:"auto",marginRight:"auto",textAlign:"center",verticalAlign:"middle",alignItems:"center",justifyContent:"center",marginTop:"20%"}}>
        <h2>Welcome to AppHero!</h2>
        <FontAwesomeIcon icon={faSpinner} spin pulse style={{height:"25%",width:"25%"}}/>
    </div>
);

export default LoadingScreen;