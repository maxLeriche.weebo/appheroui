import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

function Item(props: { to: string,txt:string, className:string,Style:{}; }) {
    return(<a className={props.className} id={props.to ? props.to:"other"}  href={"/"+((props.to&&props.to!="home") ? props.to:"")} style={{textDecoration:"none"}}> <h1 style={props.Style}>{props.txt}</h1></a>)
  }
  export default Item;

