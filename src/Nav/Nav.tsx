import { scaleRotate as Menu } from 'react-burger-menu'
import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoffee,faEnvelope,faSignInAlt,faSignOutAlt } from '@fortawesome/free-solid-svg-icons'
import Item from "./MenuItem";

type SampleProps ={
  LogedIn:Boolean,
  Pseudo:string,
  Image:string,
}

class Nav extends React.Component<SampleProps> {
  constructor(props:SampleProps){
    super(props)
  }

  render () {
    var item=this.props.LogedIn?7:3;
    const styleItem={
      marginTop:(70/item)+"%",
      marginBottom:(70/item)+"%",
      background:"#4b4d59",
      textAlign:"center",
      color:"#373a47",
    }


    return (
      <Menu styles={ styles } pageWrapId={ "page-wrap" } outerContainerId={ "outer-container" } >
        <div style={{display:"block"}}>
          <h1 style={{margin:"0"}}><a id="contact" href="/contact" style={{top:"1px"}}><FontAwesomeIcon icon={faEnvelope} style={{top:"1px",left:"1px",position: "relative", color:"white", clear: "both",display: "block",height:"24px",width:"24px"}} /></a></h1>
          <a href={this.props.LogedIn?"/acc":"/"}><img alt="Apphero logo" src={process.env.PUBLIC_URL + '/maskable_icon.png'} style={{maxWidth:"50%",borderRadius:"50%",marginLeft:"auto",marginRight:"auto",display:"block"}} /> </a>
          <h3 style={{textAlign:'center',color:"white",textTransform:"none"}}><a href={this.props.LogedIn?"/acc":"/"}>AppHero</a></h3>
          <div style={{height:"100%"}}>
          <Item to="home" txt="Home" className="menu-item" Style={styleItem}/>
          {this.props.LogedIn?<Item to="play" txt="Play" className="menu-item"  Style={styleItem}/>:""}
          {this.props.LogedIn?<Item to="chat" txt="Chat" className="menu-item"  Style={styleItem}/>:""}
          <Item to={this.props.LogedIn?"acc":"login"} txt={this.props.LogedIn?"Account":"Login"} className="menu-item"  Style={styleItem}/>
         <Item to="about" className="menu-item" txt="News about us" Style={styleItem}/>
          </div>
        </div>
        <div style={{lineHeight:"0.01",bottom:"0",position:"absolute",textAlign:"center",alignContent:"middle",fontSize:"x-small",left:"25%"}}>
        <p>Made with</p>
        <p>{"< 3 and too much "}<FontAwesomeIcon icon={faCoffee}/></p>
        <p>By batleforc</p> 
        </div>
        <a href={this.props.LogedIn?"/disconnect":"/login"}><FontAwesomeIcon icon={this.props.LogedIn?faSignOutAlt:faSignInAlt} style={{bottom:"5px",right:"5px",position: "absolute", color:"white", clear: "both",display: "block",height:"24px",width:"24px"}} /></a>

      </Menu>
    );
  }
}

var styles = {
    bmBurgerButton: {
      position: 'fixed',
      width: '20px',
      height: '17px',
      left: '10px',
      top: '10px',

    },
    bmBurgerBars: {
      background: '#FFFFFF'
    },
    bmBurgerBarsHover: {
      background: '#a90000'
    },
    bmCrossButton: {
      height: '24px',
      width: '24px'
    },
    bmCross: {
      background: '#FFFFFF'
    },
    bmMenuWrap: {
      position: 'fixed',
      height: '100%',
      overflow:'hidden'
    },
    bmMenu: {
      background: '#373a47',
      fontSize: '1.15em',
      overflow:'hidden'
    },
    bmMorphShape: {
      fill: '#373a47'
    },
    bmItemList: {
      color: '#b8b7ad',
      padding: '0.8em'
    },
    bmItem: {
      display: 'inline-block'
    },
    bmOverlay: {
      background: 'rgba(0, 0, 0, 0.3)',
     
    }
  }

export default Nav;