import React from "react";
import {Redirect} from "react-router-dom";
import * as api from "../api";

function RegisterForm(props:{logedin:Function,login:boolean}) {
    const [email, setEmail] = React.useState("");
    const [password, setPassword] = React.useState("");
    const [nom, setNom] = React.useState("");
    const [prenom, setPrenom] = React.useState("");
    const [pseudo, setPseudo] = React.useState("");
    const [error,setError] = React.useState("");

    const handleSubmit = async (e: React.FormEvent) => {
      e.preventDefault();
      await api.register(email,password,pseudo,nom,prenom)
      .then(function(data){
        if(data.startsWith("ID:")){
          window.location.replace("/login");
          return;
        }
        setError(data);
        return;
      })
    }
    if(props.login){
        return( <Redirect to="/" />)
    }
    return (
      <form onSubmit={handleSubmit} style={{paddingTop:"3em"}}>
         <img alt="Apphero logo" src={process.env.PUBLIC_URL + '/maskable_icon.png'} style={{maxWidth:"25%",borderRadius:"50%",marginLeft:"auto",marginRight:"auto",display:"block"}} /> 
         <span style={{width:"80%",marginLeft:"10%",color:"red"}}>{error}</span>
        <div>
        <label htmlFor="email" style={{width:"80%",marginLeft:"10%"}}>Email</label>
        <input
          type="email"
          id="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          style={{width:"80%",marginLeft:"10%"}}
        />
        </div>
        <div>
        <label htmlFor="password" style={{width:"80%",marginLeft:"10%"}}>Password</label>
        <input
          type="password"
          id="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          style={{width:"80%",marginLeft:"10%"}}
        />
        </div><div>
        <label htmlFor="pseudo" style={{width:"80%",marginLeft:"10%"}}>Pseudo</label>
        <input
          type="text"
          id="pseudo"
          value={pseudo}
          onChange={(e) => setPseudo(e.target.value)}
          style={{width:"80%",marginLeft:"10%"}}
        />
        </div>

        <div>
        <label htmlFor="nom" style={{width:"80%",marginLeft:"10%"}}>Nom</label>
        <input
          type="text"
          id="nom"
          value={nom}
          onChange={(e) => setNom(e.target.value)}
          style={{width:"80%",marginLeft:"10%"}}
        />
        </div><div>
        <label htmlFor="prenom" style={{width:"80%",marginLeft:"10%"}}>Prenom</label>
        <input
          type="text"
          id="prenom"
          value={prenom}
          onChange={(e) => setPrenom(e.target.value)}
          style={{width:"80%",marginLeft:"10%"}}
        />
        </div>        

        <a href="/login" style={{width:"30%",textAlign:"center",marginLeft:"35%",marginTop:"2.5%",display:"block"}}>Login</a>
        <button style={{width:"30%",textAlign:"center",marginLeft:"35%",marginTop:"2.5%",display:"block"}}>Register</button>
      </form>
    );
  }

  export default RegisterForm;
