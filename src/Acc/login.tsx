import React from "react";
import {Redirect} from "react-router-dom";
import * as api from "../api";
function LoginForm(props:{logedin:Function,login:boolean}) {
    const [email, setEmail] = React.useState("");
    const [password, setPassword] = React.useState("");
    const [error,setError] = React.useState("");

    const handleSubmit = async (e: React.FormEvent) => {
         e.preventDefault();
        await api.login(email,password).then(function(data:string){
          if(data.startsWith("ID:")){
            console.log("Login")
            if(data) props.logedin(true);
            window.location.reload(false);
          }
          console.log(data);
            setError(data);
        })
    }
    if(props.login){
        return( <Redirect to="/" />)
    }
    return (
      <form onSubmit={handleSubmit} style={{paddingTop:"3em"}}>
         <img alt="Apphero logo" src={process.env.PUBLIC_URL + '/maskable_icon.png'} style={{maxWidth:"25%",borderRadius:"50%",marginLeft:"auto",marginRight:"auto",display:"block"}} /> 

        <div>
        <span style={{width:"80%",marginLeft:"10%",color:"red"}}>{error}</span>
        <label htmlFor="email" style={{width:"80%",marginLeft:"10%"}}>Email</label>
        <input
          type="email"
          id="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          style={{width:"80%",marginLeft:"10%"}}
        />
        </div>
        <div>
        <label htmlFor="password" style={{width:"80%",marginLeft:"10%"}}>Password</label>
        <input
          type="password"
          id="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          style={{width:"80%",marginLeft:"10%"}}
        />
        </div>
        <a href="/register" style={{width:"30%",textAlign:"center",marginLeft:"35%",marginTop:"2.5%",display:"block"}}>register</a>
        <input type="button" style={{width:"30%",textAlign:"center",marginLeft:"35%",marginTop:"2.5%",display:"block"}} value="Login" onClick={handleSubmit}/>
      </form>
    );
  }

  export default LoginForm;
