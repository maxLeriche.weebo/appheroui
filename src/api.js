var AddAPI= "https://api.weebo.fr/";


var RenewToken=""
var userID=""
var status=""
export async function login(mail,password){
    return await fetch(AddAPI+"acc/login",{method:'POST',headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },body:JSON.stringify({mail:mail,pass:password})})
    .then(res => res.ok?res.json():res)
    .then(function(data){
        if("ok" in data){
            return (data.status==404?"User not found":data.status==403?"combo user password doesnt work":data.status)+" "+data.statusText;
        }
         console.log(data);
         RenewToken=data.token;
         userID=data.userId;
         SaveRENEWToken(data.token);
         return "ID:"+userID;
    })
    .catch(function(data){console.log("API unavailable"+data);status="error";return "error"})
    }
export async function register(mail,password,pseudo,nom,prenom){
    return await fetch(AddAPI+"acc/register",{method:'POST',headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },body:JSON.stringify({mail:mail,pass:password,pseudo:pseudo,nom:nom,prenom:prenom})})
        .then(res => res.ok?res.json():res)
        .then(function(data){
            if("ok" in data){
                console.log(data)
                return data;
            }
             console.log(data);
             return "ID:"+data.userID;
        })
        .catch(function(data){console.log("API unavailable"+data);status="error"; return "error"})
    }
export function SaveRENEWToken(token){
    localStorage.setItem("RENEWtoken",token)
}
export function GetRENEWToken(){
    return localStorage.getItem("RENEWtoken")
}

export function GetTokenFromRenewToken(){
    return await fetch(AddAPI+"acc/Token",{method:'POST',headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization':'Bearer'+GetRENEWToken()
      }})
    .then(res => res.ok?res.json():res)
    .then(function(data){
        if("ok" in data){
            console.log(data)
            return data;
        }
         console.log(data);
         SetToken(data.token);
         return
    })
    .catch(function(data){console.log("API unavailable"+data);status="error"; return "error"})
}

export function GetTokenFromStorage(){
    return  localStorage.getItem("token");
}

export function SetToken(token){
    localStorage.setItem("token",token);
}