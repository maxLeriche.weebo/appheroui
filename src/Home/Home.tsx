import React from "react";
import './main.css'

const Home: React.FC = () => (
  <div  id="page-wrap">
  {/* Banner */}
  <div id="banner">
    <h2><strong>AppHero :</strong>Plate forme d'accès à des jeux dont vous etes le héro</h2>
    <p className="">Basé sur "le livre dont vous êtes le héro", voici l'application dont vous serez le héro </p>
    <a href="#" className="button large icon solid fa-check-circle">Créer mon compte !</a>
  </div>
  {/* Main Wrapper */}
  <div id="main-wrapper">
    <div className="wrapper style1">
      <div className="inner">
        {/* Feature 1 */}
        <section className="container box feature1">
          <div className="row">
            <div className="col-12">
              <header className="first major">
                <h2>This is an important heading</h2>
                <p>And this is where we talk about why we’re <strong>pretty awesome</strong> ...</p>
              </header>
            </div>
            <div className="col-4 col-12-medium">
              <section>
                <a href="#" className="image featured"><img src="images/pic01.jpg" alt="" /></a>
                <header className="second icon solid fa-user">
                  <h3>Here's a Heading</h3>
                  <p>And a subtitle</p>
                </header>
              </section>
            </div>
            <div className="col-4 col-12-medium">
              <section>
                <a href="#" className="image featured"><img src="images/pic02.jpg" alt="" /></a>
                <header className="second icon solid fa-cog">
                  <h3>Also a Heading</h3>
                  <p>And another subtitle</p>
                </header>
              </section>
            </div>
            <div className="col-4 col-12-medium">
              <section>
                <a href="#" className="image featured"><img src="images/pic03.jpg" alt="" /></a>
                <header className="second icon solid fa-chart-bar">
                  <h3>Another Heading</h3>
                  <p>And yes, a subtitle</p>
                </header>
              </section>
            </div>
            <div className="col-12">
              <p>Phasellus quam turpis, feugiat sit amet ornare in, hendrerit in lectus. Praesent semper
                bibendum ipsum, et tristique augue fringilla eu. Vivamus id risus vel dolor auctor euismod
                quis eget mi. Etiam eu ante risus. Aliquam erat volutpat. Aliquam luctus mattis lectus sit
                amet pulvinar. Nam nec turpis.</p>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div></div>
);

export default Home;