# AppHeroUI

AppheroUi est l'interface graphique lier aux projet APPHERO et fonctionne avec AppHeroAPI.

## Techno

ReactJS en typescript, du css/js/html (oui dans cette ordre la)

## Usage

```shel
npm install

npm start

```

une fois fais go en localhost:3000

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## REPO PUBLIC

[REPO](https://gitlab.com/maxLeriche.weebo/appheroui) [Make MD](https://www.makeareadme.com/#mind-reading)
